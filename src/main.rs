
extern crate regex;

extern crate csv;
extern crate "rustc-serialize" as rustc_serialize;
extern crate encoding;

use std::path::Path;
use std::fmt;
use std::io;
use encoding::{Encoding, DecoderTrap};
use encoding::all::WINDOWS_1251;
use std::str::FromStr;

#[derive(Show, RustcDecodable)]
struct TbRecord {
    account: String,
    date: String,
    date_processing: String,
    sum_in_currency: f64,
    currency: String,
    sum_in_rubles: f64,
    account_currency: String,
    description: String,
    status: String,
}

#[derive(Show, RustcDecodable)]
struct HmRecord_intl {
    date: String,
    account : String,
    category : String,
    total : String,
    currency : String,
    description : String,
    transfer : String,
}

#[derive(Show, RustcDecodable)]
struct HmRecord {
    date: String,
    account : String,
    category : String,
    total : f64,
    currency : String,
    description : String,
    transfer : String,
}

fn parse_tb_date(s : &str) -> String
{
    let re = match regex::Regex::new(r"^(\d{4})-(\d{2})-(\d{2}) .*$") {
        Ok(re) => re,
        Err(err) => panic!("{:?}", err),
    };

    let cap = re.captures(s).unwrap();
    let yy : &str = cap.at(1).unwrap_or("");
    let mm : &str = cap.at(2).unwrap_or("");
    let dd : &str = cap.at(3).unwrap_or("");
    return format!("{}-{}-{}", yy, mm, dd)
}

#[test]
fn test_parse_tb_date()
{
    let r = parse_tb_date("2015-01-22 08:46:07");
    assert_eq!(r, "2015-01-22")
}

fn parse_hm_date(s : &str) -> String
{
    let re = match regex::Regex::new(r"^(\d{2}).(\d{2}).(\d{4})$"){
        Ok(re) => re,
        Err(err) => panic!("{:?}", err),
    };
    let cap = re.captures(s).unwrap();
    return format!("{}-{}-{}", cap.at(3).unwrap(), cap.at(2).unwrap(), cap.at(1).unwrap())
}

#[test]
fn test_parse_hm_date()
{
    let r = parse_tb_date("01.06.2014");
    assert_eq!(r, "2014-06-01");
}

fn read_tb(fpath : &Path) -> Vec<TbRecord>
{
    let mut file = io::File::open(fpath);
    let raw_content = match file.read_to_end() {
        Ok(x) => x,
        Err(e) => panic!("error reading: {}", e),
    };

    let content = match WINDOWS_1251.decode(raw_content.as_slice(), DecoderTrap::Strict) {
        Ok(x) => x,
        Err(e) => panic!("failed to decode: {}", e),
    };

    let bytes = content.into_bytes();
    let mut buf = io::BufReader::new(bytes.as_slice());

    for i in range(0, 11) {
        let l = match buf.read_line() {
            Ok(x) => x,
            Err(e) => panic!("failed to read (skip) line: {}", e),
        };
        println!("skipping '{}'", l.trim());
    }

    let mut rdr = csv::Reader::from_reader(buf).delimiter(b';').flexible(true);

    let mut ret : Vec<TbRecord> = vec![];

    for record in rdr.decode() {
        let mut record: TbRecord = record.unwrap();
        record.date = parse_tb_date(record.date.as_slice());
        ret.push(record)
    }

    return ret;
}

fn read_hm(fpath : &Path) -> Vec<HmRecord>
{
    let mut file = io::File::open(fpath);

    let mut rdr = csv::Reader::from_file(fpath).delimiter(b';');

    let mut ret : Vec<HmRecord> = vec![];


    for record in rdr.decode() {
        let ri: HmRecord_intl = record.unwrap();

        let total1 : String = ri.total.replace(",", ".");
        let total_f = match FromStr::from_str(total1.as_slice()) {
            Some(t) => t,
            None => panic!("Failed converting '{}' from str.", total1),
        };

        let mut r: HmRecord = HmRecord{
            date: ri.date,
            account : ri.account,
            category : ri.category,
            total : total_f,
            currency : ri.currency,
            description : ri.description,
            transfer : ri.transfer,
        };

        ret.push(r)
    }

    return ret;
}

fn main()
{
    let fpath = &Path::new("/Users/ruslan/Downloads/statement.csv");
    let hm_path = &Path::new("/Users/ruslan/Downloads/31.01.2015@12.44.05.export.csv");

    let tb_recs = read_tb(fpath);

    for record in tb_recs.iter() {
        // println!("{} {}", record.status, record.sum_in_rubles);
    }

    let hm_recs = read_hm(hm_path);

    for record in hm_recs.iter() {
        println!("{} {} {}", record.date, record.account, record.total);
    }


}
